"use strict";

const assert = require("assert");
const iprivpg = require("bindings")("iprivpg");

// Error codes
const errCodes = [
    {errCode: -1, errName:'CRYPT_ERR_BAD_ARGS', errDescr: 'Ошибка в аргументах'},
    {errCode: -2, errName:'CRYPT_ERR_OUT_OF_MEMORY', errDescr: 'Ошибка выделения памяти'},
    {errCode: -3, errName:'CRYPT_ERR_INVALID_FORMAT', errDescr: 'Неверный формат документа'},
    {errCode: -4, errName:'CRYPT_ERR_NO_DATA_FOUND', errDescr: 'Документ прочитан не до конца'},
    {errCode: -5, errName:'CRYPT_ERR_INVALID_PACKET_FORMAT', errDescr: 'Ошибка во внутренней структуре документа'},
    {errCode: -6, errName:'CRYPT_ERR_UNKNOWN_ALG', errDescr: 'Неизвестный алгоритм шифрования'},
    {errCode: -7, errName:'CRYPT_ERR_INVALID_KEYLEN', errDescr: 'Длина ключа не соответствует длине подписи'},
    {errCode: -8, errName:'CRYPT_ERR_INVALID_PASSWD', errDescr: 'Неверная кодовая фраза закрытого ключа'},
    {errCode: -9, errName:'CRYPT_ERR_DOCTYPE', errDescr: 'Неверный тип документа'},
    {errCode:-10, errName:'CRYPT_ERR_RADIX_DECODE', errDescr: 'Ошибка ASCII кодирования документа'},
    {errCode:-11, errName:'CRYPT_ERR_RADIX_ENCODE', errDescr: 'Ошибка ASCII декодирования документа'},
    {errCode:-12, errName:'CRYPT_ERR_INVALID_ENG', errDescr: 'Неизвестный тип криптосредства'},
    {errCode:-13, errName:'CRYPT_ERR_ENG_NOT_READY', errDescr: 'Криптосредство не готово'},
    {errCode:-14, errName:'CRYPT_ERR_NOT_SUPPORT', errDescr: 'Вызов не поддерживается криптосредством'},
    {errCode:-15, errName:'CRYPT_ERR_FILE_NOT_FOUND', errDescr: 'Файл не найден'},
    {errCode:-16, errName:'CRYPT_ERR_CANT_READ_FILE', errDescr: 'Ошибка чтения файла'},
    {errCode:-17, errName:'CRYPT_ERR_INVALID_KEY', errDescr: 'Ключ не может быть использован'},
    {errCode:-18, errName:'CRYPT_ERR_SEC_ENC', errDescr: 'Ошибка формирования подписи'},
    {errCode:-19, errName:'CRYPT_ERR_PUB_KEY_NOT_FOUND', errDescr: 'Открытый ключ с таким серийным номером отсутствует'},
    {errCode:-20, errName:'CRYPT_ERR_VERIFY', errDescr: 'Подпись не соответствует содержимому документа'},
    {errCode:-21, errName:'CRYPT_ERR_CREATE_FILE', errDescr: 'Ошибка создания файла'},
    {errCode:-22, errName:'CRYPT_ERR_CANT_WRITE_FILE', errDescr: 'Ошибка записи в файл'},
    {errCode:-23, errName:'CRYPT_ERR_INVALID_KEYCARD', errDescr: 'Неверный формат карточки ключа'},
    {errCode:-24, errName:'CRYPT_ERR_GENKEY', errDescr: 'Ошибка генерации ключей'},
    {errCode:-25, errName:'CRYPT_ERR_PUB_ENC', errDescr: 'Ошибка шифрования'},
    {errCode:-26, errName:'CRYPT_ERR_SEC_DEC', errDescr: 'Ошибка дешифрации'},
    {errCode:-27, errName:'CRYPT_ERR_UNKNOWN_SENDER', errDescr: 'Отправитель неопределен'}
];

function findErrByCode(errCode) {
    return errCodes.find(element => element.errCode === errCode);
}

const Crypto = function (settings, logger) {
    assert(settings);
    assert(settings.secretKey);
    assert(settings.secretPhrase);

    var log = function() {
        if (logger) {
            logger.log(arguments[0], arguments[1]);
        }
    };

    var rc = iprivpg.initialize();
    if (rc !== 0) {
        log('init unsuccessful', rc);
        throw new Error('Crypto library is not initialized');
    }

    var secretKey = new iprivpg.IprivKey();
    rc = secretKey.OpenSecretKeyFromFile(settings.secretKey, settings.secretPhrase);
    if (rc !== 0) {
        log('cannot open secret key from file', rc);
        const err = findErrByCode(rc);
        throw new Error(`Can not open secret key from file. ERR_NAME: "${err.errName}" ERR_DESCR: "${err.errDescr}"`);
    }
    
    var publicKey = new iprivpg.IprivKey();
    rc = publicKey.OpenPublicKeyFromFile(settings.publicKey, settings.publicSerial);
    if (rc !== 0) {
        log('cannot open public key from file', rc);
        const err = findErrByCode(rc);
        throw new Error(`Can not open public key from file. ERR_NAME: "${err.errName}" ERR_DESCR: "${err.errDescr}"`);
    }
    
    var sign = function(message) {

        var result = Buffer.alloc(message.length + 4096, 0, 'binary');
        
        try {
            var rc = secretKey.Sign(message, result);

            if (rc > 0) {
                return Buffer.from(result, 0, rc);
            } else {
                console.log("cannot sign message", rc, message);
                log("cannot sign message", rc, message);
                throw new Error('cannot sign message');
            }
        } catch (e) {
            if(/RC:(.*)$/.test(e.message)) {
                const v = /RC:(.*)$/.exec(e.message);
                if(v && v.length === 2) {
                    const errCode = parseInt(v[1]);
                    const err = findErrByCode(errCode);
                    throw new Error(`ERROR: ${e.message}. ERR_NAME: "${err.errName}" ERR_DESCR: "${err.errDescr}"`);
                }
            } else {
                throw new Error(e.message);
            }
        }
    };
    
    var validate = function(message) {

        var result = Buffer.alloc(message.length, 0, 'binary');
        
        try {
            var rc = publicKey.Verify(message, result);

            if (rc > 0) {
                return Buffer.from(result, 0, rc);
            } else {
                console.log("cannot verify message", rc, message);
                log("cannot verify message", rc, message);
                return false;
            }
        } catch (e) {
            if(/RC:(.*)$/.test(e.message)) {
              const v = /RC:(.*)$/.exec(e.message);
              if(v && v.length === 2) {
                  const errCode = parseInt(v[1]);
                  const err = findErrByCode(errCode);
                  throw new Error(`ERROR: ${e.message}. ERR_NAME: "${err.errName}" ERR_DESCR: "${err.errDescr}"`);
              }
            } else {
                throw new Error(e.message);
            }
        }
    };

    return {
        sign: sign,
        validate: validate
    };
};

module.exports = Crypto;
