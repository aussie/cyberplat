# Cyberplat 

The node js module for make payments through the cyberplat.ru service

[![Build Status](https://travis-ci.org/antirek/cyberplat.svg?branch=master)](https://travis-ci.org/antirek/cyberplat)

## Prerequisite

There is necessary to carry out a cryptographic signature of the message to make payments via the cyberplat service. The "libipriv" library is uses for signature.

## Installation

> npm install

> npm run test

> node paytest.js

## Example

`````javascript

var Cyberplat = require('cyberplat');
var moment = require('moment');
var randomstring = require("randomstring");

var cyberplat = new Cyberplat({
    crypto: {
        secretKey: './secret/secret.key',  //path to secret.key
        secretPhrase: ''                   //secret password of secret key
    },
    settings: {
        SD: 17031,
        AP: 17032,
        OP: 17033
    },
    providers: {
        "227": {
            payCheck: 'https://service.cyberplat.ru/cgi-bin/t2/t2_pay_check.cgi',
            pay: 'https://service.cyberplat.ru/cgi-bin/t2/t2_pay.cgi',
            payStatus: 'https://service.cyberplat.ru/cgi-bin/es/es_pay_status.cgi'
        }
    },
    logger: console                  
});

var session = randomstring.generate(20);    // the session must be unique

var obj = {
    DATE: moment().format("DD.MM.YYYY HH:mm:ss"),
    AMOUNT: "1.00",
    AMOUNT_ALL: "1.00",
    COMMENT: "комментарий",
    NUMBER: "9135292926",
    SESSION: session
};

cyberplat.payCheck("227", obj, function(answer) {
    console.log("payCheck answer:", answer);
    
    if (answer.ERROR == "0" && answer.RESULT == "0") {
        cyberplat.pay("227", obj, function(answer) {
            console.log("pay answer:", answer);

        }
    }
});

`````

### Explanations for example

1. To make payments, you need to set the module settings.

2. There are several logical sections in the settings: crypto, settings, providers, logger.

3. The parameters necessary for the cryptographic signature are indicated in the crypto section: path to the libipriv.so module, path to the secret key, passphrase. The secret key and passphrase are obtained in the service cyberplat.ru.

4. In the settings section, the SD, AP, OP settings are indicated - the codes of the counterparty, the point of reception, and the operator of the point of reception. Then these parameters are used in all messages to make payments.

5. In the providers section, addresses for each type of request to the cyberplat.ru service are indicated.

6. In the logger section, a logging object is passed (for example, console)

7. Types of requests to the cyberplat.ru service:

-a. payCheck - request for permission to pay
-b. pay - request for payment
-c. payStatus - request for payment status


* In accordance with the [Guide for programmatic interaction with the Cyberplat system] (http://www.cyberplat.ru/download/API_CyberPlat.pdf), paragraphs 2.2, 2.3, 2.4


8. For each type of request, each provider in the providers section must have a corresponding url. For example:
`````javascript

"227": {
    payCheck: 'https://service.cyberplat.ru/cgi-bin/t2/t2_pay_check.cgi',
    pay: 'https://service.cyberplat.ru/cgi-bin/t2/t2_pay.cgi',
    payStatus: 'https://service.cyberplat.ru/cgi-bin/es/es_pay_status.cgi'
}

`````

* Example of a list of providers: ./misc/providers.json, obtained from the [list of Cyberplat providers] (https://service.cyberplat.ru/cgi-bin/view_stat.utf/help.cgi)

9. Then, when calling the request, the provider code is indicated, the cyberplat module takes the corresponding url and according to the procedure described in clause 3 of the [Cyberplat system software manual] (http://www.cyberplat.ru/download/API_CyberPlat.pdf )