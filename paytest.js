'use strict';

const Cyberplat = require('./lib/cyberplat');
const moment = require('moment');
const randomstring = require("randomstring");

const ops_tst = {
    crypto: {
        secretKey: './tests/secret.key',  // private key
        secretPhrase: '1111111111',       // private key password
        publicKey: './tests/pubkeys.key', // public key
        publicSerial: 64182               // public key serial
    },
    settings: {
        SD: 17031,
        AP: 17032,
        OP: 17034
    },
    providers: {
        "227": {
            payCheck: 'https://service.cyberplat.ru/cgi-bin/t2/t2_pay_check.cgi',
            pay: 'https://service.cyberplat.ru/cgi-bin/t2/t2_pay.cgi',
            payStatus: 'https://service.cyberplat.ru/cgi-bin/es/es_pay_status.cgi'
        },
        "228": {
            payCheck: 'https://ru-demo.cyberplat.com/cgi-bin/es/es_pay_check.cgi',
            pay: 'https://ru-demo.cyberplat.com/cgi-bin/es/es_pay.cgi',
            payStatus: 'https://ru-demo.cyberplat.com/cgi-bin/es/es_pay_status.cgi'
        },
        "000": {
            payCheck: 'http://payment.cyberplat.ru/cgi-bin/es/es_pay_check.cgi',
            pay: 'http://payment.cyberplat.ru/cgi-bin/es/es_pay.cgi',
            payStatus: 'http://payment.cyberplat.ru/cgi-bin/es/es_pay_status.cgi'
        }
    },
    logger: console
}

const ops_prd = {
    crypto: {
        secretKey: './secret/Kapi150180/secret.key', // private key
        secretPhrase: 'gtptrade',                    // private key password
        publicKey: './secret/Kapi150180/pubkeys.key', // public key
        publicSerial: 98238771                        // public key serial
    },
    settings: {
        SD: 141570,
        AP: 150180,
        OP: 150181
    },
    providers: {
        // Altel
        "2" : {
            payCheck: 'https://kz.cyberplat.com/cgi-bin/al/al_pay_check.cgi',
            pay: 'https://kz.cyberplat.com/cgi-bin/al/al_pay.cgi',
            payStatus: ' https://kz.cyberplat.com/cgi-bin/al/al_pay_status.cgi'
        },
        // Tele2
        "52" : {
            payCheck: 'https://kz.cyberplat.com/cgi-bin/up/up_pay_check.cgi/52',
            pay: 'https://kz.cyberplat.com/cgi-bin/up/up_pay.cgi/52',
            payStatus: 'https://kz.cyberplat.com/cgi-bin/up/up_pay_status.cgi'
        },
        // KCell
        "113": {
            payCheck: 'https://kz.cyberplat.com/cgi-bin/up/up_pay_check.cgi/113',
            pay: 'https://kz.cyberplat.com/cgi-bin/up/up_pay.cgi/113',
            payStatus: 'https://kz.cyberplat.com/cgi-bin/up/up_pay_status.cgi'
        },
        // Activ
        "114": {
            payCheck: 'https://kz.cyberplat.com/cgi-bin/up/up_pay_check.cgi/114',
            pay: 'https://kz.cyberplat.com/cgi-bin/up/up_pay.cgi/114',
            payStatus: 'https://kz.cyberplat.com/cgi-bin/up/up_pay_status.cgi'
        },
        // payment provider with MNP feature (universal provider for all MSISDN in KZ)
        "mnp": {
            payCheck: 'https://kz.cyberplat.com/cgi-bin/mnp/mnp_pay_check.cgi',
            pay: 'https://kz.cyberplat.com/cgi-bin/mnp/mnp_pay.cgi',
            payStatus: 'https://kz.cyberplat.com/cgi-bin/mnp/mnp_pay_status.cgi'
        }
    },
    logger: console
}


const cyberplat = new Cyberplat(ops_prd);


/**
 * Make payment for the given mobile phone number
 *
 * @param msisdn mobile phone number
 * @param ammount ammount in format #.##
 * @param comment comment
 * @param providerId provider ID. Default value is "mnp"
 * @param checkPaymentStatus Need to check payment status or not
 */
function makePayment(msisdn, ammount, comment, providerId = "mnp", checkPaymentStatus = false) {
    const session = randomstring.generate(20); // the session should be unique

    let obj = {
        DATE: moment().format("DD.MM.YYYY HH:mm:ss"),
        AMOUNT: ""+ammount,
        AMOUNT_ALL: ""+ammount,
        COMMENT: comment,
        NUMBER: msisdn,
        SESSION: session
    };

    cyberplat.payCheck(providerId, obj, function (a1) {
        console.log("payCheck answer:", a1);

        if (a1.ERROR === "0" && a1.RESULT === "0") {
            cyberplat.pay(providerId, obj, function (a2) {
                console.log("pay answer:", a2);

                if (a2.ERROR === "0" && a2.RESULT === "0" && checkPaymentStatus) {
                    // add TRANSID to the request object
                    obj.TRANSID = a2.TRANSID;

                    cyberplat.payStatus(providerId, obj, (a3) => {
                        console.log('Pay status:', a3);
                    })
                }
            });
        }
    });
}

function checkPayment() {
    obj.TRANSID = '1000661790258';

    cyberplat.payStatus("52", obj, (result) => {
        console.log("pay status: ", result);
    });
};

makePayment("7077306394","1.00", "test payment","mnp",true);
// checkPayment();